FROM alpine:latest

MAINTAINER Edward Muller <edward@heroku.com>

WORKDIR "/opt"

ADD .docker_build/cloud_oblig1 /opt/bin/cloud_oblig1

CMD ["/opt/bin/cloud_oblig1"]

/*Collaboration:
- Kent Holt
- Eldar Hauge Torkelsen
- Jonas Solsvik
*/
package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sort"
	"testing"
)

func Test_handler(t *testing.T) {
	mock = true
	testcase := []struct {
		repo string
		own  string
	}{
		{
			repo: "kafka",
			own:  "apache",
		},
	}

	ts := httptest.NewServer(http.HandlerFunc(handler))
	defer ts.Close()

	res, err := http.Get(ts.URL + "/projectinfo/v1/github.com/apache/kafka")

	if err != nil {
		t.Errorf("Error making the GET request, %s", err)
	}

	s, err := ioutil.ReadAll(res.Body)

	if err != nil {
		t.Errorf("Error converting response to expected bytes. Got error: %s", err)
	}

	defer res.Body.Close()

	err = json.Unmarshal(s, &mockp)

	if err != nil {
		t.Errorf("Error parsing the expected JSON body. Got error: %s", err)
	}

	if mockp.Own != testcase[0].own {
		t.Errorf("Language recieved is not correct, got %s need %s", mockp.Own, testcase[0].own)
	}

	if mockp.Pro != testcase[0].repo {
		t.Errorf("Language recieved is not correct, got %s need %s", mockp.Pro, testcase[0].repo)
	}

}

func Test_getLanguage(t *testing.T) {
	mock = true
	testcase := []struct {
		name string
	}{
		{name: "Batchfile"},
		{name: "HTML"},
		{name: "Java"},
		{name: "Python"},
		{name: "Scala"},
		{name: "Shell"},
		{name: "XSLT"},
	}
	mock = true
	ts := httptest.NewServer(http.HandlerFunc(getLanguages))
	defer ts.Close()

	res, err := http.Get(ts.URL + "/projectinfo/v1/github.com/apache/kafka")

	if err != nil {
		t.Errorf("Error making the GET request, %s", err)
	}

	s, err := ioutil.ReadAll(res.Body)

	if err != nil {
		t.Errorf("Error converting response to expected bytes. Got error: %s", err)
	}

	defer res.Body.Close()

	err = json.Unmarshal(s, &mockp)

	sort.Strings(mockp.Lang)

	if err != nil {
		t.Errorf("Error parsing the expected JSON body. Got error: %s", err)
	}

	for i := 0; i < len(mockp.Lang); i++ {
		if mockp.Lang[i] != testcase[i].name {
			t.Errorf("Language recieved is not correct, got %s need %s", mockp.Lang[i], testcase[i])
		}
	}

}

func Test_GetTopCommitter(t *testing.T) {
	mock = true
	testcaseCommitter := []struct {
		Comm  string
		Commi int
	}{
		{
			Comm:  "ijuma",
			Commi: 312,
		},
	}

	mock = true
	ts := httptest.NewServer(http.HandlerFunc(getTopCommitter))

	defer ts.Close()

	res, err := http.Get(ts.URL + "/projectinfo/v1/github.com/apache/kafka/")

	if err != nil {
		t.Errorf("Error making the GET request, %s", err)
	}

	s, err := ioutil.ReadAll(res.Body)

	if err != nil {
		t.Errorf("Error converting response to expected bytes. Got error: %s", err)
	}

	defer res.Body.Close()

	err = json.Unmarshal(s, &mockp)

	if err != nil {
		t.Errorf("Error parsing the expected JSON body. Got error: %s", err)
	}

	if mockp.Comm != testcaseCommitter[0].Comm {
		t.Errorf("Top committer does not match, %s /d %s", mockp.Comm, testcaseCommitter[0].Comm)
	}

	if mockp.Commi != testcaseCommitter[0].Commi {
		t.Errorf("Top committer does not match, %d /d %d", mockp.Commi, testcaseCommitter[0].Commi)
	}

}

/*Collaboration:
- Kent Holt
- Eldar Hauge Torkelsen
- Jonas Solsvik
*/
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// Payload (Used to create the json object with data, which is required for the assignment)
type Payload struct {
	Pro   string   `json:"project, omitempty"`
	Own   string   `json:"owner, omitempty"`
	Comm  string   `json:"committer, omitempty"`
	Commi int      `json:"commits, omitempty"`
	Lang  []string `json:"language, omitempty"`
}

//Commit (json object)
type Commit struct {
	Login         string `json:"login"`
	Contributions int    `json:"contributions"`
}

//Repo (json object)
type Repo struct {
	RepoName string `json:"name"`
	Own      Owner  `json:"owner"`
}

//Owner (json object)
type Owner struct {
	Owner string `json:"login"`
}

// Global variable
var p Payload
var c []Commit
var repo Repo
var mockp Payload
var mock = false
var githubAPIUrl = "https://api.github.com/repos/"

func handler(w http.ResponseWriter, r *http.Request) {
	switch mock {
	case true:
		res, err := ioutil.ReadFile("repo.json")

		if err != nil {
			log.Fatal(fmt.Fprintln(w, http.StatusBadRequest))
		}

		err = json.Unmarshal(res, &repo)

		if err != nil {
			panic(err)
		}

		mockp.Pro = repo.RepoName
		mockp.Own = repo.Own.Owner

		response, err := json.MarshalIndent(mockp, "", "  ")

		if err != nil {
			panic(err)
		}

		fmt.Fprintln(w, string(response))

	case false:
		vars := mux.Vars(r)
		username := vars["username"]
		reponame := vars["reponame"]
		p.Pro = reponame
		p.Own = username

		getLanguages(w, r)
		getTopCommitter(w, r)

		response, err := json.MarshalIndent(p, "", "  ")

		if err != nil {
			panic(err)
		}

		fmt.Fprintln(w, string(response))
	}
}

/*
According to https://developer.github.com/v3/repos/#list-contributors
the URL https://api.github.com/repos/{username}/{reponame}/contributors contains the list
of contributors, sorted by the number of commits per contributor in descending order.
That is why in the first json object in the c[] will be the top committer.
mock = true is only used for testing purposes
mock = false is defined globaly which sends request and get response from github api
*/
func getTopCommitter(w http.ResponseWriter, r *http.Request) {
	switch mock {
	case true:
		res, err := ioutil.ReadFile("contributors.json")

		if err != nil {
			log.Fatal(fmt.Fprintln(w, http.StatusBadRequest))
		}

		err = json.Unmarshal(res, &c)

		if err != nil {
			panic(err)
		}

		mockp.Comm = c[0].Login
		mockp.Commi = c[0].Contributions

		response, err := json.MarshalIndent(mockp, "", "  ")

		if err != nil {
			panic(err)
		}

		fmt.Fprintln(w, string(response))

	case false:

		url := githubAPIUrl + p.Own + "/" + p.Pro + "/contributors" // URL for contributors

		res, err := http.Get(url)

		if err != nil {
			log.Fatal(fmt.Fprintln(w, http.StatusBadRequest))
		}

		defer res.Body.Close()

		err = json.NewDecoder(res.Body).Decode(&c)

		if err != nil {
			panic(err)
		}
		// setting top committer's name to the globalaly defined payload struct
		p.Comm = c[0].Login

		p.Commi = c[0].Contributions
	}

}

/* Get languages used in a certain github repository.
   mock = true case is only used for testing purposes
   mock = false is defined globaly which sends request and get response from github api
*/
func getLanguages(w http.ResponseWriter, r *http.Request) {

	switch mock {
	case true:
		var mockLanguageMap map[string]interface{}

		res, err := ioutil.ReadFile("languages.json")

		if err != nil {
			log.Fatal(fmt.Fprintln(w, http.StatusBadRequest))
		}

		err = json.Unmarshal(res, &mockLanguageMap)

		if err != nil {
			panic(err)
		}

		mockp.Lang = []string{}
		for key := range mockLanguageMap {
			mockp.Lang = append(mockp.Lang, key)
		}

		response, err := json.MarshalIndent(mockp, "", "  ")

		if err != nil {
			panic(err)
		}

		fmt.Fprintln(w, string(response))

	case false:
		var lang map[string]interface{}

		url := githubAPIUrl + p.Own + "/" + p.Pro + "/languages" //url for languages

		res, err := http.Get(url)

		if err != nil {
			http.Error(w, "Malformed URL", http.StatusBadRequest)
		}

		defer res.Body.Close()

		err = json.NewDecoder(res.Body).Decode(&lang) //Unmarshalling the bytes into lang map

		if err != nil {
			panic(err)
		}

		p.Lang = []string{}

		// getting only the key from the map and adding the key to globalaly defined payload sturct
		for key := range lang {

			p.Lang = append(p.Lang, key)
		}
	}

}

func main() {

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/projectinfo/v1/{github}/{username}/{reponame}/", handler)

	//port := os.Getenv("PORT")
	//log.Println(http.ListenAndServe(":"+port, router))
	log.Println(http.ListenAndServe("localhost:8080", router))

}
